﻿using CliFx;
using CliFx.Attributes;
using EightPuzzle.Algorithms;
using System;
using System.Threading.Tasks;

namespace EightPuzzle.Commands
{
    [Command("breadthfirst")]
    class BreadthFirstCommand : ICommand
    {
        [CommandOption("state", 's', Description = "Initial state. Pass as numbers separated by dots")]
        public string InitialState { get; set; }

        public ValueTask ExecuteAsync(IConsole console)
        {
            var initialState = PuzzleState.FromIdentity(this.InitialState);
            if (initialState is null)
            {
                console.PrintError($"Given input identity could no be used to construct the initial state. Format is 'Number.Number.Number...Number.Number'\n");
                return default;
            }
            IAlgorithm algorith = new BreadthFirstSearch(PuzzleState.SolvedState);
            console
                .PrintColored($"Initial Parameters:\n", ConsoleColor.White)
                .PrintColored($"{initialState.Value.Identity}\n", ConsoleColor.Gray);
            console.PrintResult(algorith.Solve(initialState.Value));
            return default;

        }
    }
}
