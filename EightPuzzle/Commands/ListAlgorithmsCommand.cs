﻿using CliFx;
using CliFx.Attributes;
using EightPuzzle.Algorithms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EightPuzzle.Commands
{
    [Command("list", Description = "List all available algorithms")]
    public class ListAlgorithmsCommand : ICommand
    {
        private IEnumerable<IAlgorithm> algorithms = new List<IAlgorithm>(){
            new BreadthFirstSearch(PuzzleState.SolvedState),
            new DepthFirstSearch(PuzzleState.SolvedState),
            new IterativeDeepeningSearch(PuzzleState.SolvedState)
        };

        public ValueTask ExecuteAsync(IConsole console)
        {
            console.PrintColored("The following algorithms can be picked\n\n", ConsoleColor.White);
            foreach(var alg in algorithms)
            {
                console
                    .PrintColored($"{alg.Name}\n", ConsoleColor.Green)
                    .PrintColored($"{alg.Description}\n\n", ConsoleColor.DarkGreen);
                     
            }

            return default;
        }
    }
}
