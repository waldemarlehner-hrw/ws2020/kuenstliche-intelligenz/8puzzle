﻿using CliFx;
using CliFx.Attributes;
using EightPuzzle.Algorithms;
using EightPuzzle.Heuristics;
using System.Threading.Tasks;

namespace EightPuzzle.Commands
{
    [Command("astar")]
    internal class AStarCommand : ICommand
    {
        [CommandParameter(0,Description = "Heuristic to be used. 'inplace' or 'manhattan'")]
        public string Heuristic { get; set; }

        [CommandOption("greedy", 'g', Description = "Should the cost to the current node be ignored?")]
        public bool Greedy { get; set; } = false;

        [CommandOption("state", 's', Description = "Initial state. Pass as numbers separated by dots")]
        public string InitialState { get; set; }

        [CommandOption("unique", 'u', Description = "Should a hashmap be used to avoid infinite loops. This is wise to use with the greedy flag")]
        public bool Hash { get; set; } = false;

        public ValueTask ExecuteAsync(IConsole console)
        {

            IHeuristic heuristic = ParseHeuristic(Heuristic);
            if(heuristic is null)
            {
                console.PrintError($"Given input heuristic could no be used to construct the initial state. Format is 'Number.Number.Number...Number.Number'\n");
                return default;
            }
            var initialState = PuzzleState.FromIdentity(this.InitialState);
            if (initialState is null)
            {
                console.PrintError($"Given input identity could no be used to construct the initial state. Format is 'Number.Number.Number...Number.Number'\n");
                return default;
            }
            IAlgorithm algorith = new AStarSearch(PuzzleState.SolvedState, heuristic, Greedy, Hash);
            console.PrintResult(algorith.Solve(initialState.Value));
            return default;
        }

        private IHeuristic ParseHeuristic(string heuristic)
        {
            switch (heuristic)
            {
                case "distance": case "manhattan":
                    return new ManhattanDistanceHeuristic();
                case "inplace": case "tileinplace":
                    return new TileInPlaceHeuristic();
                default:
                    return null;
            }
        }
    }
}
