﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace EightPuzzle
{
    internal static class PuzzleStateHelper
    {
        const int LEN = 3;

        internal static (int x, int y) GetCoordinatesFromIndex(int i, int width) => (i % width, i / width);

        internal static int GetIndexFromCoordinates((int x, int y) coordinates, int width) => coordinates.x + coordinates.y * width;

        internal static IList<PuzzleState.MoveAction> GetPossibleMoveActions(int index, (int width, int height) dimensions, PuzzleState.MoveAction actionThatLeadToThis)
        {
            var actionToRemoveIfItExists = actionThatLeadToThis switch
            {
                PuzzleState.MoveAction.DOWN => PuzzleState.MoveAction.UP,
                PuzzleState.MoveAction.UP => PuzzleState.MoveAction.DOWN,
                PuzzleState.MoveAction.LEFT => PuzzleState.MoveAction.RIGHT,
                PuzzleState.MoveAction.RIGHT => PuzzleState.MoveAction.LEFT,
                _ => PuzzleState.MoveAction.NONE
            };
            var list = GetPossibleMoveActions(index, dimensions);
            list.Remove(actionToRemoveIfItExists);
            return list;
        }

        internal static IList<PuzzleState.MoveAction> GetPossibleMoveActions(int index, (int width, int height) dimensions){
            var (x, y) = PuzzleStateHelper.GetCoordinatesFromIndex(index, dimensions.width);
            List<PuzzleState.MoveAction> allowedActions = new List<PuzzleState.MoveAction>(4);
            if (x != dimensions.width - 1)
            {
                allowedActions.Add(PuzzleState.MoveAction.RIGHT);
            }
            if (x != 0)
            {
                allowedActions.Add(PuzzleState.MoveAction.LEFT);
            }
            if (y != 0)
            {
                allowedActions.Add(PuzzleState.MoveAction.UP);
            }
            if (y != dimensions.height - 1)
            {
                allowedActions.Add(PuzzleState.MoveAction.DOWN);
            }
            return allowedActions;
        }

        internal static int GetSwapIndex(int currentEmpty, int width, PuzzleState.MoveAction action)
        {
            if(action == PuzzleState.MoveAction.DOWN)
            {
                return currentEmpty + width;
            }

            if(action == PuzzleState.MoveAction.UP)
            {
                return currentEmpty - width;
            }

            if(action == PuzzleState.MoveAction.LEFT)
            {
                return currentEmpty - 1;
            }

            if(action == PuzzleState.MoveAction.RIGHT)
            {
                return currentEmpty + 1;
            }

            throw new ArgumentException("Given Action is not valid", nameof(action));
        }

        internal static string GenerateIdentity(int[] state)
        {
            StringBuilder sb = new StringBuilder();
            foreach(var i in state)
            {
                sb.Append(i).Append(".");
            }
            sb.Length--;
            return sb.ToString();
        }

        internal static PuzzleState GenerateFromIdentity(string state)
        {
            var split = state.Trim().Split('.');
            if (split.Length != LEN * LEN)
            {
                throw new ArgumentException("Given input state is invalid as it does not have the required quantity of elements.");
            }
            List<int> indices = BuildArrayFromIdentity(split);
            CheckIdentityFromIntegrity(indices);

            var zeroindex = indices.FindIndex(e => e == 0);
            if (zeroindex == -1)
            {
                throw new ArgumentException("Given identity string does not have an occurence of ZERO");
            }
            return new PuzzleState(indices.ToArray(), zeroindex);

        }

        private static void CheckIdentityFromIntegrity(List<int> indices)
        {
            var distinctCount = indices.Distinct().Count();
            var boundsFilterCount = indices.Where(e => e >= 0 && e <= LEN * LEN - 1).Count();
            if (distinctCount != boundsFilterCount || distinctCount != indices.Count)
            {
                throw new ArgumentException("Input Contains data that has either indices out of range or duplicate indices");
            }
        }

        private static List<int> BuildArrayFromIdentity(string[] split)
        {
            List<int> indices = new List<int>();
            foreach (var input in split)
            {
                var canParse = int.TryParse(input, out var output);
                if (canParse)
                {
                    indices.Add(output);
                }
                else
                {
                    throw new ArgumentException("Input Contains Data that is not an int.");
                }
            }

            return indices;
        }
    }
}
