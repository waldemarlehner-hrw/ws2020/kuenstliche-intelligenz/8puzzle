﻿using System;
using System.Collections.Generic;

namespace EightPuzzle
{
    public interface IAlgorithmResult
    {
        public bool IsSuccessFull { get; }
        public TimeSpan TimeToFinish { get; }
        public IList<PuzzleState> SolutionChain { get; }
        public ulong TouchedNodes { get; }
        public ulong NodesInStorage { get; }
    }

    public interface IAlgorithmHeuristicResult : IAlgorithmResult
    {
        public bool IsGreedy { get; }
        new public IList<(PuzzleState state, uint costHere, uint costToSolution, uint summedCost)> SolutionChain { get; }
    }
}
