﻿using CliFx;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EightPuzzle
{
    internal static class ConsoleOutputHelper
    {
        public static IConsole PrintColored(this IConsole console, string text, ConsoleColor color)
        {
            console.WithForegroundColor(color, () => console.Output.Write(text));
            return console;
        }

        public static IConsole PrintError(this IConsole console, string text) 
            => PrintColored(console, "ERR: \t" + text, ConsoleColor.Red);

        public static IConsole PrintResult(this IConsole console, IAlgorithmResult result)
        {
            const ConsoleColor KEY = ConsoleColor.White;
            const ConsoleColor VALUE = ConsoleColor.Gray;

            console
                .PrintColored("\n\n\nDid find solution? \t", KEY)
                .PrintColored(result.IsSuccessFull ? "yes" : "no", result.IsSuccessFull ? ConsoleColor.DarkGreen : ConsoleColor.DarkRed)
                .PrintColored("\nTime Taken\t", KEY)
                .PrintColored($"{result.TimeToFinish.TotalSeconds} s", VALUE)
                .PrintColored("\nNodes in Storage\t", KEY)
                .PrintColored($"{result.NodesInStorage} Nodes", VALUE)
                .PrintColored("\nNodes touched\t", KEY)
                .PrintColored($"{result.TouchedNodes} Nodes\n\n\n", VALUE);
            if(result is IAlgorithmHeuristicResult)
            {
                var isGreedy = (result as IAlgorithmHeuristicResult).IsGreedy;
                console.PrintColored("Greedy?\t", KEY);
                console.PrintColored((isGreedy) ? "yes\n\n" : "no\n\n", (isGreedy) ? ConsoleColor.Green : ConsoleColor.Red);
            }
            if (result.SolutionChain.Count > 0)
            {
                (int id, int state, int action) mainColumnsLen = GenerateMainColumns(result.SolutionChain);
                (int total, int toNode, int toFinish)? heuristicColumnsLen = null;
                bool greedy = false;
                if(result is IAlgorithmHeuristicResult)
                {
                    IEnumerable<(uint total, uint toNode, uint toFinish)> collection = (result as IAlgorithmHeuristicResult).SolutionChain.Select(e => (total: e.summedCost, toNode: e.costHere, toFinish: e.costToSolution));
                    heuristicColumnsLen = GenerateHeuristicColumn(collection);
                    greedy = (result as IAlgorithmHeuristicResult).IsGreedy;
                }

                PrintHeading(console, mainColumnsLen, heuristicColumnsLen, !greedy);
                console.Output.Write("\n");
                if(result is IAlgorithmHeuristicResult)
                {
                    
                    for (int i = 0; i < (result as IAlgorithmHeuristicResult).SolutionChain.Count; i++)
                    {
                        var state = (result as IAlgorithmHeuristicResult);
                        var entry = state.SolutionChain[i];
                        PrintMainRow(console, result.SolutionChain[i], i + 1, mainColumnsLen);
                        PrintHeuristicRow(console, state.IsGreedy, (int)entry.summedCost, (int)entry.costHere, (int)entry.costToSolution, heuristicColumnsLen.Value);
                        console.Output.Write("\n");
                    }
                    
                }
                else
                {
                    for(int i = 0; i < result.SolutionChain.Count; i++)
                    {
                        var entry = result.SolutionChain[i];
                        PrintMainRow(console, entry, i + 1, mainColumnsLen);
                        console.Output.Write("\n");
                    }
                }


            }
            return console;
                
        }

        private static void PrintMainRow(IConsole console, PuzzleState state, int index, (int id, int state, int action) mainColumnsLen)
        {
            string id = index.ToString();
            string action = Enum.GetName(typeof(PuzzleState.MoveAction), state.ActionThatLeadHere);

            console
                .PrintColored(new string(' ', mainColumnsLen.id - id.Length) + id + " ", ConsoleColor.Gray)
                .PrintColored(new string(' ', mainColumnsLen.action - action.Length) + action +" ", ConsoleColor.DarkYellow)
                .PrintColored(new string(' ', mainColumnsLen.state - state.Identity.Length) + state.Identity + " ", ConsoleColor.DarkCyan);
        }

        private static void PrintHeuristicRow(IConsole console, bool greedy, int totalCost, int g, int h, (int total, int toNode, int toFinish) maxLen)
        {
            string total = totalCost.ToString();
            console.PrintColored(new string(' ', maxLen.total - total.Length) + total + " ", ConsoleColor.DarkMagenta);
            if (!greedy)
            {
                console.PrintColored(new string(' ', maxLen.toNode - g.ToString().Length) + g.ToString() + " ", ConsoleColor.DarkRed);
                console.PrintColored(new string(' ', maxLen.toFinish - h.ToString().Length) + h.ToString() + " ", ConsoleColor.DarkRed);
            }

        }

        private static void PrintHeading(IConsole console, (int id, int state, int action) mainColumnsLen, (int total, int toNode, int toFinish)? heuristicColumnsLen, bool hasHAndGInfo)
        {
            console
                .PrintColored(new string(' ', mainColumnsLen.id - "#".Length) + "# ", ConsoleColor.White)
                .PrintColored(new string(' ', mainColumnsLen.action - "ACTION".Length) + "ACTION ", ConsoleColor.Yellow)
                .PrintColored(new string(' ', mainColumnsLen.state - "STATE".Length) + "STATE ", ConsoleColor.Cyan);
            
            if(!(heuristicColumnsLen is null))
            {
                console.PrintColored(new string(' ', heuristicColumnsLen.Value.total - "TOTAL".Length) + "TOTAL ", ConsoleColor.Magenta);
                if (hasHAndGInfo)
                {
                    console.PrintColored(new string(' ', heuristicColumnsLen.Value.toNode - "g(x)".Length) + "g(x) ", ConsoleColor.Red);
                    console.PrintColored(new string(' ', heuristicColumnsLen.Value.toFinish - "h(x)".Length) + "h(x) ", ConsoleColor.Red);
                }
            }
        }

        private static (int id, int state, int action) GenerateMainColumns(ICollection<PuzzleState> solutionChain)
        {
            (int id, int state, int action) lengths = ("#".Length, "STATE".Length, "ACTION".Length);
            lengths.id = Max(lengths.id, (int)Math.Floor(Math.Log10(solutionChain.Count) + 1));
            foreach(var entry in solutionChain)
            {
                lengths.state = Max(lengths.state, entry.Identity.Length);
                lengths.action = Max(lengths.action, Enum.GetName(typeof(PuzzleState.MoveAction), entry.ActionThatLeadHere).Length);
            }
            return lengths;
        }

        private static (int total, int toNode, int toFinish) GenerateHeuristicColumn(IEnumerable<(uint costHere, uint costToSolution, uint summedCost)> heuristics)
        {
            (int total, int toNode, int toFinish) length = ("TOTAL".Length, "g(x)".Length, "h(x)".Length);
            foreach(var (costHere, costToSolution, summedCost) in heuristics)
            {
                length.total = Max(length.total, summedCost.ToString().Length);
                length.toNode = Max(length.toNode, costHere.ToString().Length);
                length.toFinish = Max(length.toFinish, costToSolution.ToString().Length);
            }
            return length;
        }

        private static int Max(this int a, int b) => (a > b) ? a : b;
    }
}
