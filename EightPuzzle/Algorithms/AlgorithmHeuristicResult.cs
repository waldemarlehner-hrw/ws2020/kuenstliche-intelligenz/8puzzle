﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EightPuzzle.Algorithms
{
    struct AlgorithmHeuristicResult : IAlgorithmHeuristicResult
    {
        public bool IsGreedy { get; set; }

        public IList<(PuzzleState state, uint costHere, uint costToSolution, uint summedCost)> SolutionChain { get; set; }

        public bool IsSuccessFull { get; set; }

        public TimeSpan TimeToFinish { get; set; }

        public ulong TouchedNodes { get; set; }

        public ulong NodesInStorage { get; set; }

        IList<PuzzleState> IAlgorithmResult.SolutionChain => new List<PuzzleState>(SolutionChain.Select((e) => e.state));
    }
}
