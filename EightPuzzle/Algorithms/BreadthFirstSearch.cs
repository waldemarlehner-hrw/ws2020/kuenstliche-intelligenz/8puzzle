﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace EightPuzzle.Algorithms
{
    public class BreadthFirstSearch : IAlgorithm
    {
        private readonly PuzzleState solvedState;
        private ConcurrentBag<NodeWrapper> currentLevel;
        private ConcurrentBag<NodeWrapper> nextLevel;
        private NodeWrapper root;
        private ulong checkedNodes = 0;
        private ulong depth = 0;

        public string Name => "breadthfirstsearch";

        public string Description => "Use Breadth First Search. Solution guaranteed to be optimal. Will find a solution if enough space is available";

        public BreadthFirstSearch(PuzzleState solvedState)
        {
            this.solvedState = solvedState;
            this.currentLevel = new ConcurrentBag<NodeWrapper>();
            this.nextLevel = new ConcurrentBag<NodeWrapper>();
        }

        public IAlgorithmResult Solve(PuzzleState initialState)
        {
            this.root = new NodeWrapper(initialState);
            this.currentLevel.Add(this.root);

            var x = initialState == solvedState;
            var y = initialState.Identity == solvedState.Identity;

            // Look at all nodes in the current depth. Check if there is a solution. 
            // If no, add the children to the nextLevel List. In the next iteration, 
            // this will become the currentLevel List
            NodeWrapper solution = null;

            Stopwatch sw = new Stopwatch();
            sw.Start();
            // The Count condition will always be > 0 in this instance because the tree is infinite.
            while (this.currentLevel.Count > 0 && solution is null)
            {
                depth++;
                checkedNodes += (ulong)this.currentLevel.Count;
                Parallel.ForEach(this.currentLevel, (e, state) =>
                {
                    if (e.State == this.solvedState)
                    {
                        solution = e;
                        // Equivalent to a break; But need to use this because we are in a parallel context.
                        state.Stop(); 
                    }
                    else
                    {
                        foreach(var action in e.State.MoveActions)
                        {
                            var wrappedChild = new NodeWrapper(e.State.TakeAction(action), e);
                            this.nextLevel.Add(wrappedChild);
                        }
                    }
                });
                // After all nodes on this depth are checked, remove the currenLevel List. 
                // It gets replaced by the nextLevel List.
                this.currentLevel.Clear();
                this.currentLevel = this.nextLevel;
                this.nextLevel = new ConcurrentBag<NodeWrapper>();
            }
            sw.Stop();
            var sucess = !(solution is null);

            return new AlgorithmResult(sw.Elapsed, GenerateSolutionChain(solution), checkedNodes, checkedNodes, sucess); 
        }

        private static List<PuzzleState> GenerateSolutionChain(NodeWrapper solution)
        {
            if(solution is null)
            {
                return new List<PuzzleState>(0);
            }
            LinkedList<PuzzleState> nodes = new LinkedList<PuzzleState>();
            NodeWrapper current = solution;
            while (current != null)
            {
                nodes.AddFirst(current.State);
                current = current.Parent;
            }
            return new List<PuzzleState>(nodes);
        }

        private class NodeWrapper
        {
            internal NodeWrapper(PuzzleState state, NodeWrapper parent = null)
            {
                this.Children = new List<NodeWrapper>();
                this.State = state;
                this.Parent = parent;
                if(!(this.Parent is null))
                {
                    this.Parent.Children.Add(this);
                }
            }

            internal PuzzleState State { get; }
            internal NodeWrapper Parent { get; }
            internal List<NodeWrapper> Children { get; }
        }


    }
}
