﻿using System.Collections.Generic;
using System.Diagnostics;

namespace EightPuzzle.Algorithms
{
    public class DepthFirstSearch : IAlgorithm
    {
        private readonly Stack<PuzzleStateWrapper> CurrentChain = new Stack<PuzzleStateWrapper>();
        private readonly PuzzleState solvedState;
        private ulong checkedNodes = 0;

        public string Name => "depthfirstsearch";

        public string Description => "Use the Depth First Search Algorithm. Solution not guaranteed. Solution may not be optimal.";

        public DepthFirstSearch(PuzzleState solvedState)
        {
            this.solvedState = solvedState;
        }

        public IAlgorithmResult Solve(PuzzleState initialState)
        {
            PuzzleStateWrapper current = new PuzzleStateWrapper(initialState);
            this.CurrentChain.Push(current);
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var result = this.DepthSearch();
            sw.Stop();
            List<PuzzleState> chainlist = GenerateDecisionChain();
            return new AlgorithmResult(sw.Elapsed, chainlist, this.checkedNodes + (ulong)chainlist.Count, (ulong)chainlist.Count, result);
        }

        private List<PuzzleState> GenerateDecisionChain()
        {
            LinkedList<PuzzleState> chainToSolution = new LinkedList<PuzzleState>();
            //Solution has been found. It is time to "dequeue" the Stack
            while (this.CurrentChain.TryPop(out var item))
            {
                chainToSolution.AddFirst(item.State);
            }
            return new List<PuzzleState>(chainToSolution);
        }

        private bool DepthSearch()
        {
            while (true)
            {
                bool canPeek = this.CurrentChain.TryPeek(out var current);
                if (!canPeek)
                {
                    // Stack is empty
                    return false;
                }
                this.checkedNodes++;
                if (current.State == this.solvedState)
                {
                    //current is the solution state
                    return true;
                }
                if (current.Children.TryPop(out var action))
                {
                    var childState = current.State.TakeAction(action);
                    this.CurrentChain.Push(new PuzzleStateWrapper(childState));
                }
                else
                {
                    // No more children to check at this node are available. Go up.
                    // This case will never happen in this scenario, as the N-Puzzle 
                    // creates an *infinite* tree, but it is still required to fully
                    // implement Depth-First-Search
                    this.CurrentChain.Pop();
                }
            }
           
        }

        private class PuzzleStateWrapper
        {
            internal PuzzleStateWrapper(PuzzleState state)
            {
                this.State = state;
                this.Children = new Stack<PuzzleState.MoveAction>(this.State.MoveActions);
            }
            internal PuzzleState State { get; }
            internal Stack<PuzzleState.MoveAction> Children { get; }
        }
    }
}
