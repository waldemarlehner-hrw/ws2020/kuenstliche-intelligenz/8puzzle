﻿using System.Collections.Generic;
using System.Diagnostics;

namespace EightPuzzle.Algorithms
{
    public class IterativeDeepeningSearch : IAlgorithm
    {
        private readonly Stack<PuzzleStateWrapper> CurrentChain = new Stack<PuzzleStateWrapper>();
        private readonly PuzzleState solvedState;
        private readonly uint maxDepth;
        private ulong checkedNodes = 0;
      

        public string Name => "iterativedeepening";

        public string Description => "Use Iterative Deepening Depth-First Search. Will find an optimal solution if it can be found within the maxDepth.";

        public IterativeDeepeningSearch(PuzzleState solvedState, uint? maxDepth = null)
        {
            this.solvedState = solvedState;
            this.maxDepth = maxDepth ?? uint.MaxValue;
        }

        public IAlgorithmResult Solve(PuzzleState initialState)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            bool result = false;
            for(int bound = 0; bound <= maxDepth; bound++)
            {
                //When here, Chain is empty
                this.CurrentChain.Push(new PuzzleStateWrapper(initialState));
                result = this.DepthSearch(bound);
                if (result)
                {
                    break;
                }
            }
            
            sw.Stop();
            List<PuzzleState> chainlist = GenerateDecisionChain();
            return new AlgorithmResult(sw.Elapsed, chainlist, this.checkedNodes, (ulong)chainlist.Count, result);
        }

        private List<PuzzleState> GenerateDecisionChain()
        {
            LinkedList<PuzzleState> chainToSolution = new LinkedList<PuzzleState>();
            //Solution has been found. It is time to "dequeue" the Stack
            while (this.CurrentChain.TryPop(out var item))
            {
                chainToSolution.AddFirst(item.State);
            }
            return new List<PuzzleState>(chainToSolution);
        }

        private bool DepthSearch(int depth)
        {
            while (this.CurrentChain.Count > 0)
            {

                bool canPeek = this.CurrentChain.TryPeek(out var current);
                if (!canPeek)
                {
                    // Stack is empty
                    return false;
                }
                this.checkedNodes++;
                if (current.State == this.solvedState)
                {
                    //current is the solution state
                    return true;
                }
                if (this.CurrentChain.Count <= depth && current.Children.TryPop(out var action))
                {
                    var childState = current.State.TakeAction(action);
                    this.CurrentChain.Push(new PuzzleStateWrapper(childState));
                }
                else
                {
                    // No more children to check at this node are available or the depth limit has been reached. 
                    // Go up.
                    this.CurrentChain.Pop();
                }
            }
            return false;

        }

        private class PuzzleStateWrapper
        {
            internal PuzzleStateWrapper(PuzzleState state)
            {
                this.State = state;
                this.Children = new Stack<PuzzleState.MoveAction>(this.State.MoveActions);
            }
            internal PuzzleState State { get; }
            internal Stack<PuzzleState.MoveAction> Children { get; }
        }
    }
}
