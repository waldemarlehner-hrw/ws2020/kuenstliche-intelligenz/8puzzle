﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EightPuzzle.Algorithms
{
    interface IAlgorithm
    {
        string Name { get; }
        string Description { get; }
        IAlgorithmResult Solve(PuzzleState initialState);
    }
}
