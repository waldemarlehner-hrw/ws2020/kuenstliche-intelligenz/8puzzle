﻿using EightPuzzle.Heuristics;
using System;
using System.Collections.Generic;
using Priority_Queue;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

namespace EightPuzzle.Algorithms
{
    public class AStarSearch : IAlgorithm
    {
        public string Name => "A Star";

        public string Description => "Solved the problem using the A* Algorith with a given heuristic";

        public PuzzleState SolvedState { get; }
        public IHeuristic Heuristic { get; }
        public bool Greedy { get; }
        public bool Hash { get; }
        private long nodeCount = 1;
        private readonly HashSet<string> checkedIdentities = new HashSet<string>();

        private readonly SimplePriorityQueue<NodeWrapper,uint> queue = new SimplePriorityQueue<NodeWrapper,uint>();

        public AStarSearch(PuzzleState solvedState, IHeuristic heuristic, bool greedy, bool hash)
        {
            this.SolvedState = solvedState;
            this.Heuristic = heuristic;
            this.Greedy = greedy;
            this.Hash = hash;
        }

        public IAlgorithmResult Solve(PuzzleState initialState)
        {
            var root = new NodeWrapper(initialState, Convert.ToUInt32(this.Heuristic.Evaluate(initialState)));
            this.checkedIdentities.Add(root.State.Identity);
            NodeWrapper solution = null;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            if(root.State == SolvedState)
            {
                solution = root;
            }
            queue.Enqueue(root, root.Heuristic);
            while(queue.TryDequeue(out var elementFromQueue) && solution is null) {
                Parallel.ForEach(elementFromQueue.State.MoveActions, (e) =>
                {
                    

                    Interlocked.Increment(ref nodeCount);
                    var state = elementFromQueue.State.TakeAction(e);
                    if (Hash && checkedIdentities.Contains(state.Identity))
                    { 
                        return;
                    }

                    var wrapped = new NodeWrapper(state, elementFromQueue, (uint)this.Heuristic.Evaluate(state));
                    if(state == SolvedState)
                    {
                        solution = wrapped;
                    }
                    else
                    {
                        if (Hash)
                        {
                            checkedIdentities.Add(state.Identity);
                        }
                        queue.Enqueue(wrapped, GetCostOfNode(wrapped, Greedy));
                    }
                });
            }
            sw.Stop();
            IList<(PuzzleState state, uint costHere, uint costToSolution, uint summedCost)> SolutionChain = GenerateSolutionChain(solution);
            return new AlgorithmHeuristicResult
            {
                IsGreedy = this.Greedy,
                IsSuccessFull = !(solution is null),
                SolutionChain = SolutionChain,
                NodesInStorage = (ulong)nodeCount,
                TimeToFinish = sw.Elapsed,
                TouchedNodes = (ulong)nodeCount
            };
        }

        private IList<(PuzzleState state, uint costHere, uint costToSolution, uint summedCost)> GenerateSolutionChain(NodeWrapper solution)
        {
            var llist = new LinkedList<(PuzzleState state, uint costHere, uint costToSolution, uint summedCost)>();
            var current = solution;
            while(current != null)
            {
                llist.AddFirst((current.State, current.Depth, current.Heuristic, GetCostOfNode(current, this.Greedy)));
                current = current.Parent;
            }
            return new List<(PuzzleState state, uint costHere, uint costToSolution, uint summedCost)>(llist);
        }

        private static uint GetCostOfNode(NodeWrapper node, bool greedy) => (greedy) ? node.Heuristic : node.Heuristic + node.Depth;


        private class NodeWrapper
        {
            internal NodeWrapper(PuzzleState root, uint heuristic)
            {
                this.State = root;
                this.Parent = null;
                this.Depth = 0;
                this.Heuristic = heuristic;
                this.Children = new List<NodeWrapper>();
            }

            internal NodeWrapper(PuzzleState state, NodeWrapper parent, uint heuristic)
            {
                this.State = state;
                this.Parent = parent;
                this.Heuristic = heuristic;
                this.Depth = this.Parent.Depth + 1;
                this.Children = new List<NodeWrapper>();
                lock (this.Parent.Children)
                {
                    this.Parent.Children.Add(this);
                }
                
            }

            internal PuzzleState State { get; }
            internal IList<NodeWrapper> Children { get; }
            internal NodeWrapper Parent { get; }
            internal uint Depth { get; }
            internal uint Heuristic { get; }
        }
    }
}
