﻿using System;
using System.Collections.Generic;

namespace EightPuzzle.Algorithms
{
    public struct AlgorithmResult : IAlgorithmResult
    {
        public AlgorithmResult(TimeSpan ttf, IList<PuzzleState> chain, ulong touched, ulong storage, bool sucess)
        {
            TimeToFinish = ttf;
            SolutionChain = chain;
            TouchedNodes = touched;
            NodesInStorage = storage;
            IsSuccessFull = sucess;
        }
        
        public bool IsSuccessFull { get; }
        public TimeSpan TimeToFinish { get; }
        public IList<PuzzleState> SolutionChain { get; }
        public ulong TouchedNodes { get; }
        public ulong NodesInStorage { get; }
    }
}