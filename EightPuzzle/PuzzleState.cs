﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EightPuzzle
{
    public struct PuzzleState
    {
        public enum MoveAction
        {
            NONE = -1,
            UP,
            DOWN,
            LEFT,
            RIGHT
        }

        const int EMPTY = 0;
        const int LEN = 3; // LEN * LEN = size of State
        private readonly int emptyIndex;

        // Generate a PuzzleState using an int array representing the state.
        public PuzzleState(int[] state, int? emptyCellIndex, PuzzleState.MoveAction actionThatLeadHere =  MoveAction.NONE)
        {
            this.emptyIndex = emptyCellIndex ?? state.Where(e => e == EMPTY).FirstOrDefault();
            if(state[emptyIndex] != 0)
            {
                throw new ArgumentException(emptyCellIndex is null ? "Given State does not have an empty cell" : "Given emptyCellIndex is incorrect");
            }
            this.State = state;
            this.ActionThatLeadHere = actionThatLeadHere;
            this.MoveActions = PuzzleStateHelper.GetPossibleMoveActions(this.emptyIndex, (LEN, LEN),actionThatLeadHere);
            this.Identity = PuzzleStateHelper.GenerateIdentity(state);
        }

            // Generate a PuzzleState from scratch
        public PuzzleState(int shuffles = 0)
        {
            // Set up the solution state, then apply random operations from there
            var state = new int[LEN * LEN];
            for(int i = 0; i < state.Length - 1;i++)
            {
                state[i] = i + 1;
            }
            state[^1] = EMPTY; //Last value shall be 0;
            this.emptyIndex = state.Length - 1;

            Random rand = new Random();
            while(shuffles-- > 0)
            {
                var actions = PuzzleStateHelper.GetPossibleMoveActions(this.emptyIndex, (LEN, LEN));
                var actionToApply = actions[(int)(actions.Count * rand.NextDouble())];

                // Get Index to swap with
                var indexToSwap = PuzzleStateHelper.GetSwapIndex(this.emptyIndex, LEN, actionToApply);
                
                // Apply swap
                state[this.emptyIndex] = state[indexToSwap];
                state[indexToSwap] = EMPTY;
                this.emptyIndex = indexToSwap;
            }

            this.State = state;
            this.MoveActions = PuzzleStateHelper.GetPossibleMoveActions(this.emptyIndex, (LEN, LEN));
            this.ActionThatLeadHere = MoveAction.NONE;
            this.Identity = PuzzleStateHelper.GenerateIdentity(this.State);

        }

        public int[] State { get;}

        public IList<MoveAction> MoveActions { get; }
        public PuzzleState.MoveAction ActionThatLeadHere { get; }
        public string Identity { get; }

        public PuzzleState TakeAction(MoveAction action)
        {
            if (!this.MoveActions.Contains(action))
            {
                throw new ArgumentException("Given Action cannot be applied to the current state", nameof(action));
            }
            var newArray = new int[LEN * LEN];
            this.State.CopyTo(newArray, 0);
            var indexToSwapWith = PuzzleStateHelper.GetSwapIndex(this.emptyIndex, LEN, action);
            newArray[this.emptyIndex] = newArray[indexToSwapWith];
            newArray[indexToSwapWith] = EMPTY;
            return new PuzzleState(newArray, indexToSwapWith, action);
        }


        public static bool operator == (PuzzleState p1, PuzzleState p2)
        {
            return p1.Identity == p2.Identity;
        }
        public static bool operator !=(PuzzleState p1, PuzzleState p2)
        {
            return p1.Identity != p2.Identity;
        }

        public static PuzzleState SolvedState => new PuzzleState(0);

        public static PuzzleState? FromIdentity(string identity)
        {
            if (!(identity is null))
            {
                try
                {
                    return PuzzleStateHelper.GenerateFromIdentity(identity);
                }
                catch
                {
                    return null;
                }
            }
            else
            {
                return new PuzzleState(10000);
            }
        }


    }
}
