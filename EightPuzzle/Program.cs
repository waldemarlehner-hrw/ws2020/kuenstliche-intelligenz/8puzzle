﻿using CliFx;
using EightPuzzle.Commands;
using System;
using System.Threading.Tasks;

namespace EightPuzzle
{
    public class Program
    {
        public static async Task<int> Main()
        {
            return await new CliApplicationBuilder()
                .AddCommand(typeof(BreadthFirstCommand))
                .AddCommand(typeof(DepthFirstCommand))
                .AddCommand(typeof(IterativeDeepeningCommand))
                .AddCommand(typeof(AStarCommand))
                .Build()
                .RunAsync();
        }
    }
}
