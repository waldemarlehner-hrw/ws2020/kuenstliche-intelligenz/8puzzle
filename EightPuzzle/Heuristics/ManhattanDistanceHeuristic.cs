﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace EightPuzzle.Heuristics
{
    public class ManhattanDistanceHeuristic : IHeuristic
    {
        public string Name => "ManhattanDistance";

        public string Description => "Use the manhattan distance for each tile (excluding blank tile)";

        public int Evaluate(PuzzleState state)
        {
            var arr = state.State;
            int cost = 0;
            Parallel.For(0, arr.Length, (index) =>
            {
                if(arr[index] != 0)
                {
                    var current = PuzzleStateHelper.GetCoordinatesFromIndex(index,3);
                    var desired = PuzzleStateHelper.GetCoordinatesFromIndex(arr[index] - 1,3);

                    var c = Math.Abs(current.x - desired.x) + Math.Abs(current.y - desired.y);
                    Interlocked.Add(ref cost, c);
                }
            });
            return cost;
        }
    }
}
