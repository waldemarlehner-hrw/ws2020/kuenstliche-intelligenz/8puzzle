﻿namespace EightPuzzle.Heuristics
{
    public interface IHeuristic
    {
        string Name { get; }
        string Description { get; }
        int Evaluate(PuzzleState state);
    }
}
