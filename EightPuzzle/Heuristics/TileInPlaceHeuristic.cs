﻿using System.Threading;
using System.Threading.Tasks;

namespace EightPuzzle.Heuristics
{
    class TileInPlaceHeuristic : IHeuristic
    {
        public string Name => "TileInPlace";

        public string Description => "Check if a tile is in its final position. If not, increase cost by one";

        public int Evaluate(PuzzleState state)
        {
            var arr = state.State;
            int cost = 0;
            Parallel.For(0, arr.Length, (i) =>
            {
                if(arr[i] == 0)
                {
                    // do nothing
                }
                else
                {
                    if(arr[i] != i + 1)
                    {
                        Interlocked.Increment(ref cost);
                    }
                }
            });
            return cost;
        }
    }
}
